//
//  ElementaryTaskFactory.swift
//  SmileMath
//
//  Created by Artem Chursin on 23.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

class ElementaryTaskFactory: TaskFactory {
    
    func produce(type: TaskType?) -> Task {
        
        return ElementaryTask(type: type)
    }
}

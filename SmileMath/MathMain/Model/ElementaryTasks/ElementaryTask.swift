//
//  ElementaryTask.swift
//  SmileMath
//
//  Created by Artem Chursin on 23.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit

class ElementaryTask: Task {
    var tasksNumbers: [Int]?
    var answer: Int?
    var taskText: String?
    var taskImage: UIImage?
    var type: TaskType?
    let numberGenerator = NumbersGenerator()
    
    //TODO: - TO TEST
    private func nod(isNeedNok: Bool) {
        
        let random = numberGenerator.generator(startCount: 10, finalCount: 40)
        let first = random * numberGenerator.generator(startCount: 12, finalCount: 30)
        let second = random * numberGenerator.generator(startCount: 12, finalCount: 30)
        
        let nod = euclidAlgorithm(firstNum: first, secondNum: second)
        
        if isNeedNok {
            let nok = first * second / nod
            answer = nok
            taskText  = "Найдите наименьшее общее кратное чисел \(first) и \(second)"
        } else {
            answer = nod
            taskText  = "Найдите наибольший общий делитель чисел \(first) и \(second)"
        }
        tasksNumbers = [first, second]
    }
    private func euclidAlgorithm(firstNum: Int, secondNum: Int) -> Int {
        
        var first = firstNum
        var second = secondNum
        var tmp: Int
        
        while second > 0 {
            
            tmp = first % second
            first = second
            second = tmp
        }
        return first
    }
    //---------------------------------------------------------------
    //TODO: - TO TEST
    private func vietCreator(parametr: Int) -> String {
        
        if parametr == 0 {
            return ""
        } else if parametr > 0 {
            return "+\(parametr)"
        } else {
            return "\(parametr)"
        }
        
    }
    
    private func vietTheorem() {
        
        // aX²+bx+c = 0
        let firstX = numberGenerator.generator(startCount: 0, finalCount: 9)
        let secondX = numberGenerator.generator(startCount: 0, finalCount: 9)
        
        let b = -(firstX + secondX)
        let c = firstX * secondX
        
        var coefb = vietCreator(parametr: b)
        if coefb != "" {
            coefb = coefb + "X"
        }
        let coefc = vietCreator(parametr: c)
        
        answer = firstX + secondX
        taskText = "Решите квадратное уравнение: X²\(coefb)\(coefc)=0. Если уравнение имеет более 2 корней, в ответ запишите их сумму."
        tasksNumbers = [firstX, secondX]
    }
    
    func generateTask() {
        
        switch type {
        case .ElementaryTask(type: .CleverGeneration):
            print("none")
        case .ElementaryTask(type: .GreatestCommonDivisor):
            nod(isNeedNok: false)
        case .ElementaryTask(type: .SmallestCommonMultiple):
            nod(isNeedNok: true)
        case .ElementaryTask(type: .VietTheorem):
            vietTheorem()
        default:
            fatalError("No elementary task type")
        }
    }
    
    init(type: TaskType?) {
        
        self.type = type
    }
}

//
//  ElementaryTaskType.swift
//  SmileMath
//
//  Created by Artem Chursin on 23.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

enum ElementaryTaskType {
    case CleverGeneration
    case VietTheorem
    case GreatestCommonDivisor
    case SmallestCommonMultiple
}

//
//  SimpleTextTaskType.swift
//  SmileMath
//
//  Created by Artem Chursin on 22.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

enum SimpleTextTaskType {
    case CleverGeneration
    case CheckInStore
    case MilesInKilometers
    case ElectricityConsumption
    case QuantityOfGoodsForMoney
    case PercentRiseInPrice
    case SalesForCompany
}

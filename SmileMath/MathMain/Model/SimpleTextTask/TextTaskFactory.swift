//
//  TextTaskFactory.swift
//  SmileMath
//
//  Created by Artem Chursin on 22.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

class SimpleTextTaskFactory: TaskFactory {
    
    func produce(type: TaskType?) -> Task {
        
        return SimpleTextTask(type: type)
    }
}

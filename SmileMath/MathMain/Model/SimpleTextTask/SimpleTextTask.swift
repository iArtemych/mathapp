//
//  SimpleTextTask.swift
//  SmileMath
//
//  Created by Artem Chursin on 22.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit

class SimpleTextTask: Task {
    var tasksNumbers: [Int]?
    var answer: Int?
    var taskText: String?
    var taskImage: UIImage?
    var type: TaskType?
    let numberGenerator = NumbersGenerator()

    
    init(type: TaskType?) {
        
        self.type = type
    }
     
    //TODO: - TODO
    private func checkInStoreTask()  {
        let text1 = "Киллограм бананов стоит "
        let text2 = "рублей. Марина купила"
        let text3 = "кг"
        let text4 = "г бананов. Сколько рублей сдачи должна получить Марина, если она отдала"
        let text5 = "рублей"
        
        taskText = text1 + text2 + text3 + text4 + text5
    }
    
    //TODO: - TO TEST
    private func quantityOfGoodsForMoney() {

        let price = numberGenerator.generator(startCount: 26, finalCount: 37)
        let cash = numberGenerator.theNumberMultipleOfhundred()
        
        let text = "Батон хлеба стоит \(price) рубля. Какое наибольшее количество батонов можно купить на \(cash) рублей?"
        
        answer = cash / price
        taskText = text
        tasksNumbers = [price, cash]
    }
    
    //TODO: - TO TEST
    private func milesInKilometers() {
        
        let nowSpeed = numberGenerator.generator(startCount: 30, finalCount: 60)
        let metersInMile = 1609
        
        var integerPart = (nowSpeed * metersInMile) / 1000
        var doublePart = (nowSpeed * metersInMile) % 1000
        var additionalPart = 0
        
        
        while doublePart > 0 {
            
            additionalPart = doublePart % 10
            doublePart = doublePart / 10
        }
        
        if additionalPart > 4 {
            integerPart += 1
        }
        
        let text = "Дмитрий Кузин купил американский автомобиль, на спидометре которого скорость измеряется в милях в час. Американская миля равна \(metersInMile) метров. Найдите скорость автомобиля в километрах в час, если спидометр показывает \(nowSpeed) мили в час? Ответ округлите до целого числа."
        answer = integerPart
        taskText = text
        tasksNumbers = [metersInMile, nowSpeed]
    }
    
    func generateTask() {
        
        switch type {
        case .SimpleTextTask(type: .CleverGeneration):
            print("none")
        case .SimpleTextTask(type: .CheckInStore):
            print("2")
        case .SimpleTextTask(type: .MilesInKilometers):
            milesInKilometers()
        case .SimpleTextTask(type: .ElectricityConsumption):
            print("4")
        case .SimpleTextTask(type: .QuantityOfGoodsForMoney):
            quantityOfGoodsForMoney()
        case .SimpleTextTask(type: .PercentRiseInPrice):
            print("6")
        case .SimpleTextTask(type: .SalesForCompany):
            print("7")
        default:
            fatalError("No simple task type")
        }
    }
    
}

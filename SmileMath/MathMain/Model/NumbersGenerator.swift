//
//  NumbersGenerator.swift
//  SmileMath
//
//  Created by Artem Chursin on 22.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

class NumbersGenerator {
    
    func generator(startCount: Int, finalCount: Int) -> Int {
        
        let randomNumber = startCount + Int(arc4random_uniform(UInt32(finalCount - startCount + 1)))
        
        return randomNumber
    }
    
    func digitNumbersGenerator() -> Int {
        
        let randomNumber = 0 + Int(arc4random_uniform(UInt32(1 - 0 + 1)))
        
        switch randomNumber {
        case 0:
            return 100
        default:
            return 1000
        }
    }
    
    func theNumberMultipleOfhundred() -> Int {
        
        let startCount = 1
        let finalCount = 9
        let randomNumber = startCount + Int(arc4random_uniform(UInt32(finalCount - startCount + 1)))
        
        return randomNumber * 100
    }
}

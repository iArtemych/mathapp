//
//  TaskFactory.swift
//  SmileMath
//
//  Created by Artem Chursin on 22.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

protocol TaskFactory {
    
    func produce(type: TaskType?) -> Task
}

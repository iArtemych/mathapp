//
//  IPadMainViewController.swift
//  SmileMath
//
//  Created by Артем Чурсин on 05/09/2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import UIKit

class IPadMainViewController: UIViewController {

    //MARK: - Constants
//---------------------------------------------------
    let names: [String] = ["Задача","Пример","График"]
    let tasksForStudent: [String] = ["Задача","Пример","График"]
    
//---------------------------------------------------
    //MARK: - Variables
    var taskMark: TaskMark = .notDone
    var answers: [Int] = [0,0,0]
    var taskNumber = 0
    
    //MARK: - Outlets
    @IBOutlet weak var taskAndPadStack: UIStackView!
    @IBOutlet weak var taskView: TaskView!
    @IBOutlet weak var keyboardAnswerView: KeyboardNumView!
    @IBOutlet weak var paintView: PaintView!
    
    //MARK: - LifeStyle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        taskView.delegate = self
        paintView.paintDelegate = self
        showDataOnViews()
        
        
        //-----------------------------
        
//        let simpleTextTaskFactory = SimpleTextTaskFactory()
//        let task = simpleTextTaskFactory.produce(cleverGeneration: true, type: .SimpleTextTask(type: .CheckInStore))
//        let task2 = simpleTextTaskFactory.produce(cleverGeneration: true, type: nil)
//        let task =
        
        //-----------------------------
    }
    
    //MARK: - Actions
    
    //MARK: - Navigation
    
    //MARK: - Methods
    func showDataOnViews() {
        
        if taskNumber > names.count - 1 {taskNumber = 0}
        if taskNumber < 0 {taskNumber = names.count - 1}
        taskView.taskName = names[taskNumber]
        taskView.taskText = tasksForStudent[taskNumber]
        
        taskView.showData()
        
        let answer = answers[taskNumber]
        keyboardAnswerView.numPadView.answerString = String(answer)
        keyboardAnswerView.numPadView.showAnswer()
    }
    
    //MARK: - Private methods

}


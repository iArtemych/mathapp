//
//  IPadMainVCDelegates.swift
//  SmileMath
//
//  Created by Artem Chursin on 03/10/2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit

extension IPadMainViewController: TaskViewDelegate {
    func didTapButton(taskmark: TaskMark?, taskNumberMark: TaskNumberMark?) {
        
        if let mark = taskmark {
            switch mark {
            case .notDone:
                taskMark = .notDone
            case .readyToСheck:
                taskMark = .readyToСheck
                print("ready")
            case .toDo:
                taskMark = .toDo
                print("toDo")
            }
        }
        
        if let mark = taskNumberMark {
            
            let strAnswer = Int(keyboardAnswerView.numPadView.answerString)
            
            guard let answer = strAnswer else {
                
                UIAlertController.showAlert(title: "Ошибка!",
                                            message: "Неверный формат ответа",
                                            inViewController: self)
                return
            }
            answers[taskNumber] = answer
            
            print(answer)
            
            switch mark {
            
            case .nextTask:
                taskNumber += 1
            case .prevTask:
                taskNumber -= 1
            }
            
            self.showDataOnViews()
            
        }
    }
}

extension IPadMainViewController: PaintViewDelegate {
    
    func didTapButton(paintButton: PaintButton, shareImage: [UIImage]?) {
                
        switch paintButton {
        case .shareButtonTap:
            self.shreTap(images: shareImage)
        case .clearButtonTap:
            resetPaintTap()
        case .fullScreenButtonTap:
            self.fullScreenTap()
    
        }
    }
    
    
    func shreTap(images: [UIImage]?) {
        
        print("2")
        guard let image = images else {
            UIAlertController.showAlert(title: "Ошибка!",
                                        message: "Ошибка сохранения изображения",
                                        inViewController: self)
            
            return
        }
        print("3")
        
        let activity = UIActivityViewController(activityItems: image, applicationActivities: nil)
        activity.popoverPresentationController?.sourceView = self.view
        self.present(activity, animated: true, completion: nil)
    }
    
    func resetPaintTap() {
        
    }
    
    func fullScreenTap() {
        
    }
    

}

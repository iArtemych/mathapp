//
//  PaintViewDelegate.swift
//  SmileMath
//
//  Created by Artem Chursin on 04/10/2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation
import UIKit

protocol PaintViewDelegate: class {

    func didTapButton(paintButton: PaintButton, shareImage: [UIImage]?)
}

//
//  Task Mark.swift
//  SmileMath
//
//  Created by Artem Chursin on 03/10/2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

enum TaskMark {
    
    case notDone
    case toDo
    case readyToСheck
}

enum TaskNumberMark {
    
    case prevTask
    case nextTask
}

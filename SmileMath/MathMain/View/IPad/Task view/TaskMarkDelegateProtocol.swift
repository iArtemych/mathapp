//
//  TaskMarkDelegateProtocol.swift
//  SmileMath
//
//  Created by Artem Chursin on 03/10/2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import Foundation

protocol TaskViewDelegate: class {

    func didTapButton(taskmark: TaskMark?, taskNumberMark: TaskNumberMark?)
}

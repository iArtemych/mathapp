//
//  SimpleTextTaskTests.swift
//  SmileMathTests
//
//  Created by Artem Chursin on 24.10.2019.
//  Copyright © 2019 Artem Chursin. All rights reserved.
//

import XCTest
import Foundation
@testable import SmileMath

class SimpleTextTaskTests: XCTestCase {
    
    
    func testMilesInKilometers(){

        let simpleTextTaskFactory = SimpleTextTaskFactory()
        let task = simpleTextTaskFactory.produce(type: .SimpleTextTask(type: .MilesInKilometers))
        
        guard let text = task.taskText,
            let answer = task.answer,
            let taskNumbers = task.tasksNumbers else {
            return
        }
        
        print(text)
        var integerPart = (taskNumbers[0] * taskNumbers[1]) / 1000
        var doublePart = (taskNumbers[0] * taskNumbers[1]) % 1000
        var additionalPart = 0
        
        
        while doublePart > 0 {
            
            additionalPart = doublePart % 10
            doublePart = doublePart / 10
        }
        
        if additionalPart > 4 {
            integerPart += 1
        }
        
        XCTAssert(integerPart == answer)        
    }
}
